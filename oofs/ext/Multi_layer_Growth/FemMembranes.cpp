#include "FemMembranes.hpp"
#include <MDXProcessFem.hpp>
#include <CCUtils.hpp>
namespace mdx
{
  bool FemMembranes::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    if(!mesh)
      throw(QString("FemMembranes::initialize No current mesh"));

    ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("FemMembranes::initialize No cell complex"));

    cs = &mesh->ccStructure(ccName);

    // Get the solver process
    if(!getProcess(parm("Solver Process"), solverProcess))
      throw(QString("FemMembranes::initialize Unable to make solver process: %1").arg(parm("Solver Process")));
    solverProcess->initialize(parent);

    // Get the growth process
    if(!getProcess(parm("Growth Process"), growthProcess))
      throw(QString("FemMembranes::initialize Unable to make growth process: %1").arg(parm("Growth Process")));
    growthProcess->initialize(parent);

    // Get the vector render processes
    /*if(!getProcess(parm("Fiber1 Render Process"), fiber1RenderProcess))
      throw QString("FemMembranes::initialize Unable to make Fiber1 render process: %1").arg(parm("Fiber1 Render Process"));
    if(!getProcess(parm("Fiber2 Render Process"), fiber2RenderProcess))
      throw QString("FemMembranes::initialize Unable to make Fiber2 render process: %1").arg(parm("Fiber2 Render Process"));
    if(!getProcess(parm("Iso Render Process"), isoRenderProcess))
      throw QString("FemMembranes::initialize Unable to make Iso render process: %1").arg(parm("Iso Render Process"));*/

    savingTimes = parm("Simulation time to save mesh");
    listSavingTimes = savingTimes.split(',');
    
     if(parm("Force Measure Process").isEmpty())
      throw QString("%1::initialize Force Measure process empty").arg(name());
    if(!getProcess(parm("Force Measure Process"), femForce))
      throw QString("%1::initialize Unable to make force measure process: %2").arg(name()).arg(parm("Force Measure Process"));
    femForce->initialize(parent);


    if(!getProcess(parm("Save Mesh Process Name"), meshSave))
        throw(QString("FemMembranes::initialize Unable to make save mesh process : %1").arg(parm("Save Mesh Process Name")));
    meshSave->initialize(parent);
    
    stopTime = parm("Simulation Stop time").toInt();

    if(!getProcess(parm("Stress-Strain Process"), femStressStrain))
      throw(QString("FemMembranes::initialize Unable to make Stress-Strain Process : %1").arg(parm("Stress-Strain Process")));


    return true;
  }

  bool FemMembranes::step()
  {
    if(!solverProcess)
      throw(QString("FemMembranes::run Solver process pointer invalid"));
    
    if(!growthProcess)
      throw(QString("FemMembranes::run Growth process pointer invalid"));   
          
    if(!femForce)
      throw QString("%1:step Force Measure process not set").arg(name());

    if(!femStressStrain)
      throw(QString("FemMembranes::step Stress-Strain process pointer invalid"));

    springEdgeAttr &springAttr = mesh->attributes().attrMap<CCIndex,springEdge>("springEdge");
    
    mesh = currentMesh();
    ccName = mesh->ccName();
    cs = &mesh->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh->indexAttr();
    
    // If converged subdivide then grow
    if(!solverProcess->step()) {

      femStressStrain->run();
      //take a screenshot
      static int screenShotCount = 0;
      meshName = parm("Name of mesh");

      QString fileName = QString("%1-%2.JPG").arg(meshName).arg(screenShotCount++, 4, 10, QChar('0'));
      takeSnapshot(fileName);
      for (int i=0; i<listSavingTimes.size(); ++i)
      {
        if(listSavingTimes[i].toInt() == screenShotCount)
        {
           QString fileNameMesh = QString("%1-%2.mdxm").arg(meshName).arg(screenShotCount, 4, 10, QChar('0'));
           meshSave->run(*mesh, fileNameMesh, true);
	         bool success = femForce->step();
           femForce->resetExtensometerStep();
           break;
        }

      }
      if (screenShotCount >= stopTime)
      return false;
	 
      growthProcess->run();
      double growthDt = growthProcess->parm("Growth Dt").toDouble();
      growthTime += growthDt;
      //if(stringToBool(solverProcess->parm("Print Stats")))
        mdxInfo << QString("Growth Step Time %1, step %2").arg(growthTime).arg(growthDt) << endl;

        mesh->updateAll(ccName);
        solverProcess->initSolver(cs);
      //}
    }
    /*if(fiber1RenderProcess)
      fiber1RenderProcess->run();
    if(fiber2RenderProcess)
      fiber2RenderProcess->run();
    if(isoRenderProcess)
      isoRenderProcess->run();*/

    mesh->updatePositions(ccName);


    return true;
  }

  bool FemMembranes::rewind(QWidget *parent)
  {
    // To rewind, we'll reload the mesh
    Mesh *mesh = currentMesh();
    if(!mesh or mesh->file().isEmpty())
      throw(QString("No current mesh, cannot rewind"));
    MeshLoad meshLoad(*this);
    meshLoad.setParm("File Name", mesh->file());
    growthTime = 0;
    return meshLoad.run();
  }

  bool FemMembranes::finalize(QWidget *parent)
  {
    if(!solverProcess)
      throw(QString("FemMembranes::run Solver process pointer invalid"));

    bool result = solverProcess->finalize(parent);

    // Cleanup
    mesh = 0;
    solverProcess = 0;
    growthProcess = 0;
   // subdivideProcess = 0;

    return result;
  }

 bool MeasurePerimeter::initialize (QWidget *parent)
 {
   mesh = currentMesh(); 
   ccName = mesh->ccName();
   perim = 0;
   cs = &mesh->ccStructure(ccName);
 }

 bool MeasurePerimeter::run ()
 {
   CCIndexDataAttr *indexAttr = &mesh->indexAttr();
   	 
   for(CCIndex e : cs->edges()) 
   {
     auto eb = cs->edgeBounds(e);
     if ((*indexAttr)[eb.first].selected == true and (*indexAttr)[eb.second].selected)
       perim = perim + norm ((*indexAttr)[eb.first].pos-(*indexAttr)[eb.second].pos);
   }
   mdxInfo << QString("perimeter")<<"  " <<perim<< endl; 
   return true;
 }  
 
 bool FemMembraneStressStrain::run(Mesh &mesh)
 {
      auto &materialAttr1 = mesh.attributes().attrMap<CCIndex, fem::TransIsoMaterial>("Fiber1 Material");

      CCIndexDoubleAttr &stressAttr1  = mesh.signalAttr<double>("fiber 1 stress");
      CCIndexDoubleAttr strainAttr1;
      CCIndexDoubleAttr &maxStressAttr1 = mesh.signalAttr<double>("fiber 1 max stress");
      CCIndexDoubleAttr maxStrainAttr;
      CCIndexDoubleAttr midStressAttr;
      CCIndexDoubleAttr midStrainAttr;
      CCIndexDoubleAttr minStressAttr;
      CCIndexDoubleAttr minStrainAttr;

      CCIndexDoubleAttr vonMisesStressAttr;
      CCIndexDoubleAttr normalStressAttr1;
      CCIndexDoubleAttr normalStrainAttr1;
      CCIndexSymTensorAttr stressAxisAttr1;
      CCIndexSymTensorAttr strainAxisAttr1;

      double axisScale = 1;
      int elementPeriod = parm("Density").toInt();
      mesh.updateProperties();
      fem::StressStrain::run(mesh, parm("Element Type"), "Fiber1 Triangle", materialAttr1, 
                                              &stressAttr1, &strainAttr1, &maxStressAttr1, &midStressAttr, &minStressAttr, &maxStrainAttr, &midStrainAttr, &minStressAttr, 
                                              &normalStressAttr1, &normalStrainAttr1, &vonMisesStressAttr, &stressAxisAttr1, &strainAxisAttr1, axisScale, elementPeriod);	 
                                              
      auto &materialAttr2 = mesh.attributes().attrMap<CCIndex, fem::TransIsoMaterial>("Fiber2 Material");

      CCIndexDoubleAttr &stressAttr2 = mesh.signalAttr<double>("fiber 2 stress");
      CCIndexDoubleAttr &maxStressAttr2 = mesh.signalAttr<double>("fiber 2 max stress");
      CCIndexDoubleAttr strainAttr2;
      CCIndexDoubleAttr normalStressAttr2;
      CCIndexDoubleAttr normalStrainAttr2;
      CCIndexSymTensorAttr stressAxisAttr2;
      CCIndexSymTensorAttr strainAxisAttr2;

      mesh.updateProperties();
      fem::StressStrain::run(mesh, parm("Element Type"), "Fiber2 Triangle", materialAttr2, 
                                              &stressAttr2, &strainAttr2, &maxStressAttr2, &midStressAttr, &minStressAttr,&maxStrainAttr, &midStrainAttr, &minStressAttr, &normalStressAttr2, &normalStrainAttr2, 
                                              &vonMisesStressAttr,  &stressAxisAttr2, &strainAxisAttr2, axisScale, elementPeriod);	                                               
                                              
                                              
      auto &materialAttr3 = mesh.attributes().attrMap<CCIndex, fem::TransIsoMaterial>("Iso Material");

      CCIndexDoubleAttr &stressAttr3 = mesh.signalAttr<double>("Iso stress");
      CCIndexDoubleAttr &maxStressAttr3 = mesh.signalAttr<double>("Iso max stress");
      CCIndexDoubleAttr strainAttr3;
      CCIndexDoubleAttr normalStressAttr3;
      CCIndexDoubleAttr normalStrainAttr3;
      CCIndexSymTensorAttr stressAxisAttr3;
      CCIndexSymTensorAttr strainAxisAttr3;

	    CCIndexSymTensorAttr combinedStressTensor;
      CCIndexDoubleAttr &combinedStress= mesh.signalAttr<double>("total stress");
      CCIndexDoubleAttr &combinedFiberMaxStress = mesh.signalAttr<double>("combinedFiber Max stress");
      mesh.updateProperties();
      fem::StressStrain::run(mesh, parm("Element Type"), "Iso Triangle", materialAttr3, 
                                              &stressAttr3, &strainAttr3, &maxStressAttr3, &midStressAttr, &minStressAttr, &maxStrainAttr, &midStrainAttr, &minStressAttr, &normalStressAttr3, &normalStrainAttr3, &vonMisesStressAttr,
                                              &stressAxisAttr3, &strainAxisAttr3, axisScale, elementPeriod);	                                              
	 
	 for (auto e: stressAxisAttr1){
		SymmetricTensor combined;
		combined.fromMatrix(stressAxisAttr3[e.first].toMatrix()+stressAxisAttr2[e.first].toMatrix() + e.second.toMatrix());
		//combined.fromMatrix(stressAxisAttr3[e.first].toMatrix() );
		combinedStressTensor[e.first] = combined;
		combinedStress[e.first] = combined.evals()*Point3d(1,1,1);
                combinedFiberMaxStress[e.first] = stressAxisAttr1[e.first].evals().x() + stressAxisAttr2[e.first].evals().x();
	 }
	 mdxInfo <<"stress strain run"<<endl;
	 return true;

     
 }
 
 
  REGISTER_PROCESS(FemMembranes);
  REGISTER_PROCESS(FemMembraneSolver);
  REGISTER_PROCESS(FemMembraneRefCfgFiber1);
  REGISTER_PROCESS(FemMembraneRefCfgFiber2);
  REGISTER_PROCESS(FemMembraneRefCfgIso);
  REGISTER_PROCESS(FemMembraneStressStrain);
  REGISTER_PROCESS(FemMembraneSetGrowth);
  REGISTER_PROCESS(FemMembraneGrowth);    
  REGISTER_PROCESS(FemMembraneDerivsFiber1);
  REGISTER_PROCESS(FemMembraneDerivsFiber2);
  REGISTER_PROCESS(FemMembraneDerivsIso);
  REGISTER_PROCESS(FemMembraneMaterialFiber1);
  REGISTER_PROCESS(FemMembraneMaterialFiber2);
  REGISTER_PROCESS(FemMembraneMaterialIso);
  REGISTER_PROCESS(FemMembraneAnisoFiber1);
  REGISTER_PROCESS(FemMembraneAnisoFiber2);
  REGISTER_PROCESS(FemMembranePressure);
  REGISTER_PROCESS(FemMembraneRenderFiber1);
  REGISTER_PROCESS(FemMembraneRenderFiber2);
  REGISTER_PROCESS(FemMembraneRenderIso);
  //REGISTER_PROCESS(MeasurePerimeter);
  REGISTER_PROCESS(FemMembraneForceMeasure);
  REGISTER_PROCESS(FemMembraneVisDirichlet);
  REGISTER_PROCESS(FemMembraneVisMaterial);
  REGISTER_PROCESS(FemMembranePressureDerivs);
  REGISTER_PROCESS(FemMembraneDirichletDerivs);
  REGISTER_PROCESS(FemMembraneSetDirichlet);
  REGISTER_PROCESS(FemMembraneComputeCurrentDirections);
  REGISTER_PROCESS(FemMembraneVisDirections);
  REGISTER_PROCESS(FemMembraneVisCumGrowth);
  REGISTER_PROCESS(FemMembraneVisGrowth);
    
}
