#ifndef FEM_MEMBRANES_HPP
#define FEM_MEMBRANES_HPP

#include <MDXProcessFem.hpp>
#include <MeshProcessSystem.hpp>
#include <MeshProcessStructure.hpp>
#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>
#include <Attributes.hpp>
#include <MeshProcessSystemRender.hpp>
#include <Solver.hpp>
#include <Process.hpp>
#include <FemTriangle.hpp>
#include <Fem.hpp>

namespace mdx
{
  class FemMembraneSolver;
  class FemMembraneGrowth;
  //class FemMembraneSubdivide; 
  //class FemMembraneStressStrainFiber2;   
  class FemMembranePressureDerivs;
  class FemMembraneForceMeasure;
  
  
  struct springEdge
  {
    double restLength = 0; 
    bool isAspring = false;
    bool visited = false;
    //std::vector<Point3d *> posVec;
    //CCIndexVec vtxVec;

    springEdge() {}
  
    bool operator==(const springEdge &other) const
    {
      if(restLength == other.restLength)
        return true;
      return false;
    }      
  };	
  typedef AttrMap<CCIndex,springEdge> springEdgeAttr;
   
  class FemMembraneRenderFiber1 : public fem::VectorRender
  {
  public:
    FemMembraneRenderFiber1(const Process &proc) : VectorRender(proc) 
    {
      setName("Model/CCF/11 Vector Render Fiber1");

      setParmDefault("Output CC", "Fiber1 Vector");
      setParmDefault("Element Attribute", "Fiber1 Triangle");
    }
  };
 
  class FemMembraneRenderFiber2 : public fem::VectorRender
  {
  public:
    FemMembraneRenderFiber2(const Process &proc) : VectorRender(proc) 
    {
      setName("Model/CCF/12 Vector Render Fiber2");

      setParmDefault("Output CC", "Fiber2 Vector");
      setParmDefault("Element Attribute", "Fiber2 Triangle");
    }
  };
    
  class FemMembraneRenderIso : public fem::VectorRender
  {
  public:
    FemMembraneRenderIso(const Process &proc) : VectorRender(proc) 
    {
      setName("Model/CCF/13 Vector Render Iso");

      setParmDefault("Output CC", "Iso Vector");
      setParmDefault("Element Attribute", "Iso Triangle");
    }
  };  

  class FemMembraneStressStrain : public fem::StressStrain
  {
  public:
    FemMembraneStressStrain(const Process &proc) : StressStrain(proc) 
    {
      setName("Model/CCF/04 Stress-Strain");
      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Iso Triangle");			
      setParmDefault("Material Attribute", "Iso Material");
      //setParmDefault("Density", "Compute stress/strain every n elements", "1");
    }
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw QString("%1::run Invalid mesh").arg(name());

      return run(*mesh);
    }
    bool run(Mesh &mesh);
  };   

  class FemMembraneDerivsFiber1 : public fem::ElementDerivs
  {
  public:
    FemMembraneDerivsFiber1(const Process &proc) : ElementDerivs(proc) 
    {
      setName("Model/CCF/021 Triangular Membranes Fiber1");
      addParm("Element Attribute", "Attribute to store nodal values", "Fiber1 Triangle");
      addParm("Material Attribute", "Name of the attribute that holds material properties", "Fiber1 Material");
      //addParm("Boundary Attribute", "Name of the attribute that holds boundary conditions", "FemTriangle Boundary");
    }
  };
  
  class FemMembraneDerivsFiber2 : public fem::ElementDerivs
  {
  public:
    FemMembraneDerivsFiber2(const Process &proc) : ElementDerivs(proc) 
    {
      setName("Model/CCF/022 Triangular Membranes Fiber2");
      addParm("Element Attribute", "Attribute to store nodal values", "Fiber2 Triangle");
      addParm("Material Attribute", "Name of the attribute that holds material properties", "Fiber2 Material");
      //addParm("Boundary Attribute", "Name of the attribute that holds boundary conditions", "FemTriangle Boundary");
    }
  };  
  class FemMembraneDerivsIso: public fem::ElementDerivs
  {
  public:
    FemMembraneDerivsIso(const Process &proc) : ElementDerivs(proc) 
    {
      setName("Model/CCF/023 Triangular Membranes Iso");
      addParm("Element Attribute", "Attribute to store nodal values", "Iso Triangle");
      addParm("Material Attribute", "Name of the attribute that holds material properties", "Iso Material");
      //addParm("Boundary Attribute", "Name of the attribute that holds boundary conditions", "FemTriangle Boundary");
    }
  }; 

  class FemMembranePressureDerivs : public fem::PressureDerivs
  {
  public:
    FemMembranePressureDerivs(const Process &proc) : PressureDerivs(proc) 
    {
      setName("Model/CCF/09 Pressure Derivs");
      setParmDefault("Element Attribute", "Iso Triangle");
    }
  };

  // Main model class
  class FemMembranes : public Process
  {
  public:
    FemMembranes(const Process &proc) : Process(proc) 
    {
      setName("Model/CCF/011 FEM Membranes");
      setDesc("FEM simulation with growth and subdivision");

      addParm("Solver Process", "Name of solver process", "Model/CCF/011 FEM Solver");
      addParm("Growth Process", "Name of growth process", "Model/CCF/08 Growth");
      addParm("Force Measure Process", "Name of process to measure pulling force", "Model/CCF/09 Pulling Force");
      //addParm("Subdivide Process", "Name of subdivision process", "Model/CCF/09 Subdivide");
      addParm("Converge Threshold", "Threshold for convergence", "10e-6");    
      addParm("Stress-Strain Process", "Name of Stress-Strain Process", "Model/CCF/04 Stress-Strain");
      //addParm("Fiber1 Render Process", "Name of process to render vectors for Fiber1 elements", "Model/CCF/11 Vector Render Fiber1");
      //addParm("Fiber2 Render Process", "Name of process to render vectors for Fiber2 elements", "Model/CCF/12 Vector Render Fiber1");
      //addParm("Iso Render Process", "Name of process to render vectors for Iso elements", "Model/CCF/13 Vector Render Iso");
      addParm("Simulation time to save mesh", "At which growth time points save the mesh -- need to be in crescent order", "49, 46, 32" );
      addParm("Save Mesh Process Name", "Name of the Process to save the mesh", "Mesh/System/Save" );
      addParm("Name of mesh", "Name of the mesh to be saved", "OvuleFEMGrowth");
      addParm("Simulation Stop time", "Time at which to stop the simulation", "20");

      
    }

    bool initialize(QWidget *parent);
    bool step();
    bool rewind(QWidget *parent);
    bool finalize(QWidget *parent);

    double growthTime = 0;
    int stopTime = 0;

  private:
    Mesh *mesh = 0;
    QString ccName;
    CCStructure *cs = 0;

    FemMembraneSolver *solverProcess = 0;
    FemMembraneGrowth *growthProcess = 0;
    //FemMembraneSubdivide *subdivideProcess = 0;
    //FemMembraneRenderFiber1 *fiber1RenderProcess = 0;
    //FemMembraneRenderFiber2 *fiber2RenderProcess = 0;
    //FemMembraneRenderIso *isoRenderProcess = 0;
    MeshSave *meshSave = 0;

    //double growthTime = 0;
    QString savingTimes;
    QStringList listSavingTimes;
    QString meshName;
    FemMembraneForceMeasure *femForce = 0;
    FemMembraneStressStrain *femStressStrain = 0;

  };
                    
  class FemMembraneSolver : public fem::FemSolver
  {
  public:
    FemMembraneSolver (const Process &proc) : fem::FemSolver(proc) 
    {
      setName("Model/CCF/011 FEM Solver");
      setDesc("FEM Simulation using triangular membrane elements");

      // Update parameters with our own defaults 
             
      addParm("Derivs Process Fiber1", "Name of process that implements solver derivatives for Fiber1", 
              "Model/CCF/021 Triangular Membranes Fiber1");
      addParm("Derivs Process Fiber2", "Name of process that implements solver derivatives for Fiber2", 
              "Model/CCF/022 Triangular Membranes Fiber2");
      addParm("Derivs Process Iso", "Name of process that implements solver derivatives for Iso", "Model/CCF/023 Triangular Membranes Iso");
      addParm("Derivs Process Pressure", "Name of process that implements solver derivatives for Pressure", "Model/CCF/09 Pressure Derivs");
      addParm("Derivs Process Springs", "Name of process that implements solver derivatives for springs", "Model/CCF/020 Spring Derivs");
      addParm("Stress-Strain Process", "Name of process that calculates the stress/strain", "Model/CCF/04 Stress-Strain");
      addParm("Dirichlet Derivs", "Process for Dirichlet derivatives", "Model/CCF/10 Dirichlet Derivs");
    }

    bool rewind(QWidget *parent)
    {
      // To rewind, we'll reload the mesh
      Mesh *mesh = currentMesh();
      if(!mesh or mesh->file().isEmpty())
        throw(QString("No current mesh, cannot rewind"));
      MeshLoad meshLoad(*this);
      meshLoad.setParm("File Name", mesh->file());
      return meshLoad.run();
    }
           
  };

  class FemMembraneRefCfgFiber1 : public fem::SetRefCfg
  {
  public:
    FemMembraneRefCfgFiber1(const Process &proc) : SetRefCfg(proc) 
    {
      setName("Model/CCF/041 Reference Configuration Fiber1");

      setParmDefault("Element Attribute", "Fiber1 Triangle");
      addParm("Thickness", "Thickness of the membrane", "1.0");
    }
  };

  class FemMembraneRefCfgFiber2 : public fem::SetRefCfg
  {
  public:
    FemMembraneRefCfgFiber2(const Process &proc) : SetRefCfg(proc) 
    {
      setName("Model/CCF/041 Reference Configuration Fiber2");

      setParmDefault("Element Attribute", "Fiber2 Triangle");
      addParm("Thickness", "Thickness of the membrane", "1.0");
    }
  };
  class FemMembraneRefCfgIso : public fem::SetRefCfg
  {
  public:
    FemMembraneRefCfgIso(const Process &proc) : SetRefCfg(proc) 
    {
      setName("Model/CCF/042 Reference Configuration iso");

      setParmDefault("Element Attribute", "Iso Triangle");
      addParm("Thickness", "Thickness of the membrane", "1.0");
    }
  };

  //class FemMembraneStressStrainFiber1 : public fem::StressStrain
  //{
  //public:
    //FemMembraneStressStrainFiber1(const Process &proc) : FemTriangleStressStrain(proc) 
    //{
      //setName("Model/CCF/04 Stress-Strain Fiber1");
    //}
  //};
  //class FemMembraneStressStrainFiber2 : public fem::StressStrain
  //{
  //public:
    //FemMembraneStressStrainFiber2(const Process &proc) : FemTriangleStressStrain(proc) 
    //{
      //setName("Model/CCF/04 Stress-Strain Fiber2");
    //}
  //}; 
  
  class FemMembraneMaterialFiber1 : public fem::SetTransIsoMaterial
  {
  public:
    FemMembraneMaterialFiber1(const Process &proc) : SetTransIsoMaterial(proc) 
    {
      setName("Model/CCF/051 Material Properties Fiber1");
      setParmDefault("Material Attribute", "Fiber1 Material");
    }
  };        

  class FemMembraneMaterialFiber2 : public fem::SetTransIsoMaterial
  {
  public:
    FemMembraneMaterialFiber2(const Process &proc) : SetTransIsoMaterial(proc) 
    {
      setName("Model/CCF/052 Material Properties Fiber2");
      setParmDefault("Material Attribute", "Fiber2 Material");
    }
  };   
  class FemMembraneMaterialIso : public fem::SetTransIsoMaterial
  {
  public:
    FemMembraneMaterialIso(const Process &proc) : SetTransIsoMaterial(proc) 
    {
      setName("Model/CCF/053 Material Properties Iso");
      setParmDefault("Material Attribute", "Iso Material");
    }
  };  

  class FemMembraneAnisoFiber1 : public fem::SetAnisoDir
  {
  public:
    FemMembraneAnisoFiber1(const Process &proc) : SetAnisoDir(proc) 
    {
      setName("Model/CCF/053 Aniso Dir Fiber1");
      setParmDefault("Element Attribute", "Fiber1 Triangle");
      setParmDefault("Direction Type", "E2");
      setParmDefault("Direction", "1.0 4.0 0.0");
    }
  };  
  class FemMembraneAnisoFiber2 : public fem::SetAnisoDir
  {
  public:
    FemMembraneAnisoFiber2(const Process &proc) : SetAnisoDir(proc) 
    {
      setName("Model/CCF/053 Aniso Dir Fiber2");
      setParmDefault("Element Attribute", "Fiber2 Triangle");
      setParmDefault("Direction Type", "E2");
      setParmDefault("Direction", "-1.0 4.0 0.0");
    }
  };

  class FemMembranePressure : public fem::SetPressure
  {
  public:
    FemMembranePressure(const Process &proc) : SetPressure(proc) 
    {
      setName("Model/CCF/08 Set Pressure");
      setParmDefault("Element Attribute", "Iso Triangle");
    }
  };

  class FemMembraneSetGrowth : public fem::SetGrowth
  {
  public:
    FemMembraneSetGrowth(const Process &proc) : SetGrowth(proc) 
    {
      setName("Model/CCF/07 Set Growth");
      setParmDefault("Element Attribute", "Iso Triangle");
    }
  };

  class FemMembraneGrowth : public fem::Grow
  {
  public:
    FemMembraneGrowth(const Process &proc) : Grow(proc) 
    {
      setName("Model/CCF/08 Growth");
      setParmDefault("Element Attribute", "Iso Triangle");
    }
  };

  class FemMembraneForceMeasure : public fem::ExtensometerDirichlet
  {
  public:
    FemMembraneForceMeasure(const Process &proc) : fem::ExtensometerDirichlet(proc) 
    {
      setName("Model/CCF/09 Pulling Force");
      setDesc("Fem Process to Measure Pulling Force");

      setParmDefault("Fem Solver", "Model/CCF/011 FEM Solver");
      setParmDefault("Dirichlet Derivs", "Model/CCF/10 Dirichlet Derivs");
    }
  };

  
  class FemMembraneVisDirichlet : public fem::VisDirichlet
  {
  public:
    FemMembraneVisDirichlet(const Process &proc) : VisDirichlet(proc) 
    {
      setName("Model/CCF/23 Visualize Dirichlet");
    }
  };
  
  class MeasurePerimeter : public Process
  {
  public:
    MeasurePerimeter(const Process &proc) : Process(proc) 
    {
      setName("Model/CCF/16 Measure perimeter");

    }
    
    bool initialize(QWidget *parent);
    bool run();  
    
  private:
    Mesh *mesh = 0;
    QString ccName;
    double perim;
    CCStructure *cs = 0;

  };    
  
  class FemMembraneVisMaterial : public fem::VisTransIsoMaterial
  {
  public:
    FemMembraneVisMaterial(const Process &proc) : VisTransIsoMaterial(proc) 
    {
      setName("Model/CCF/24 Visualize Material");
    }
  };

  class FemMembraneVisGrowth : public fem::VisGrowth
  {
  public:
    FemMembraneVisGrowth(const Process &proc) : VisGrowth(proc) 
    {
      setName("Model/CCF/25 Visualize Growth");
    }
  };

  
  
  class FemMembraneSetDirichlet : public fem::SetDirichlet
  {
  public:
    FemMembraneSetDirichlet(const Process &proc) : SetDirichlet(proc) 
    {
      setName("Model/CCF/09 Set Dirichlet");
    }
  };

  class FemMembraneDirichletDerivs : public fem::DirichletDerivs
  {
  public:
    FemMembraneDirichletDerivs(const Process &proc) : DirichletDerivs(proc) 
    {
      setName("Model/CCF/10 Dirichlet Derivs");
    }
  };  
    class FemMembraneComputeCurrentDirections : public fem::ComputeCurrentDirections
  {
    public:
      FemMembraneComputeCurrentDirections(const Process &proc) : ComputeCurrentDirections(proc)
      {
        setName("Model/CCF/17 Compute Current Directions") ;
      }
  };

  class FemMembraneVisDirections : public fem::VisDirections
  {
    public:
      FemMembraneVisDirections(const Process &proc) : VisDirections(proc)
      {
        setName("Model/CCF/18 Visualize Directions") ;
      }
  };

  class FemMembraneVisCumGrowth : public fem::CumulativeGrowthStrain
  {
    public:
      FemMembraneVisCumGrowth(const Process &proc) : CumulativeGrowthStrain(proc)
      {
        setName("Model/CCF/19 Visualize Cumulative Growth Strain or Deformation Strain") ;
      }
  }; 
 

    




}
#endif

