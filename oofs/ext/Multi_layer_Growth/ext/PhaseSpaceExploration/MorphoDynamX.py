######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:06:46

Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_assigned.mdxm', 'no', 'no')
Process.Model__CCF__11_Vector_Render_Fiber1('Fiber1 Vector', 'Material Anisotropy', 'Max', '1.0', '1.0', '0.001', 'Fiber1 Triangle')
Process.Model__CCF__12_Vector_Render_Fiber2('Fiber2 Vector', 'Material Anisotropy', 'Max', '1.0', '1.0', '0.001', 'Fiber2 Triangle')
Process.Tools__System__Load_View('/local/gmosca/CardaminePaperSimulations/multiLayer_growth/PhaseSpaceExploration/Angle_14/FemMembranesCrossedFibers.mdxv')
######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:23:32
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_fibersAngle0_assigned.mdxm', 'no', 'no')
Process.Mesh__System__Reset('')
Process.Stack__System__Delete_Stack('Stack2')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_fibersAngle0_assigned.mdxm', 'no', 'no')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Load_View('')
Process.Mesh__System__Open('', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_crossedFibers_corrected.mdxm', 'no', 'no')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_crossedFibers_Angle14_corrected.mdxm', 'no', 'no')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:33:37
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/FemMembranes.mdxv')
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:35:08
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/FemMembranes.mdxv')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/FemMembranes.mdxv')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/line_26_50_10_assigned_2000_150_p0d7_crossedFibersAngle5_corrected.mdxm', 'no', 'no')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/FemMembranes.mdxv')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/PhaseSpaceExploration/line_26_50_10_assigned_2000_150_p0d7_crossedFibersAngle10_corrected.mdxm', 'no', 'no')
Process.Model__CCF__011_FEM_Solver('50', '0.01', '1e-6', '50', '.1', '50', '0.1', 'Backward Euler', 'Preconditioned Bi-conjugate Gradient Stabilized', '500', '1e-40', 'Yes', '1e-5', 'Model/CCF/04 Stress-Strain', 'Model/CCF/021 Triangular Membranes Fiber1', 'Model/CCF/022 Triangular Membranes Fiber2', 'Model/CCF/023 Triangular Membranes Iso', 'Model/CCF/09 Pressure Derivs', '', 'Model/CCF/04 Stress-Strain', 'Model/CCF/10 Dirichlet Derivs')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1503: 2024-01-10 21:24:22
Process.Tools__System__Load_View('FemMembranes.mdxv')
