######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:06:46

Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_assigned.mdxm', 'no', 'no')
Process.Model__CCF__11_Vector_Render_Fiber1('Fiber1 Vector', 'Material Anisotropy', 'Max', '1.0', '1.0', '0.001', 'Fiber1 Triangle')
Process.Model__CCF__12_Vector_Render_Fiber2('Fiber2 Vector', 'Material Anisotropy', 'Max', '1.0', '1.0', '0.001', 'Fiber2 Triangle')
Process.Tools__System__Load_View('/local/gmosca/CardaminePaperSimulations/multiLayer_growth/PhaseSpaceExploration/Angle_14/FemMembranesCrossedFibers.mdxv')
######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:23:32
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_fibersAngle0_assigned.mdxm', 'no', 'no')
Process.Mesh__System__Reset('')
Process.Stack__System__Delete_Stack('Stack2')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_corrected_fibersAngle0_assigned.mdxm', 'no', 'no')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Load_View('')
Process.Mesh__System__Open('', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_crossedFibers_corrected.mdxm', 'no', 'no')
Process.Mesh__System__Open('Stack1', '/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/line_26_50_10_assigned_2000_150_p0d7_crossedFibers_Angle14_corrected.mdxm', 'no', 'no')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
######## New MorphoDynamX session v2.0 r2-1298: 2022-11-23 19:34:19
Process.Tools__System__Load_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Save_View('/local/gmosca/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1407: 2023-03-27 11:44:31
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/ext/Fibers2DistinctAngles/FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1407: 2023-03-27 11:47:08
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Tools__System__Save_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1407: 2023-04-11 14:23:10
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Model__CCF__011_FEM_Membranes('Model/CCF/011 FEM Solver', 'Model/CCF/08 Growth', 'Model/CCF/09 Pulling Force', '10e-6', 'Model/CCF/04 Stress-Strain', '30, 35, 40', 'Mesh/System/Save', 'FibersGrowth_26_50_10_crossedFibersAngle14And26d6_2000_150_p0d7_thick0d4doubl_thres1e-6_refined', '41')
Process.Model__CCF__11_Vector_Render_Fiber1('Fiber1 Vector', 'Material Anisotropy', 'Max', '.5', '1.0', '.01', 'Fiber1 Triangle')
Process.Model__CCF__11_Vector_Render_Fiber1('Fiber1 Vector', 'Material Anisotropy', 'Max', '.5', '1.0', '.01', 'Fiber1 Triangle')
Process.Model__CCF__12_Vector_Render_Fiber2('Fiber2 Vector', 'Material Anisotropy', 'Max', '0.5', '1.0', '.01', 'Fiber2 Triangle')
Process.Mesh__System__Delete_Cell_Complex('Stack1', 'Fiber2 Vector')
Process.Mesh__System__Delete_Cell_Complex('Stack1', 'Fiber1 Vector')
Process.Model__CCF__011_FEM_Membranes('Model/CCF/011 FEM Solver', 'Model/CCF/08 Growth', 'Model/CCF/09 Pulling Force', '10e-6', 'Model/CCF/04 Stress-Strain', '30, 35, 40', 'Mesh/System/Save', 'FibersGrowth_26_50_10_crossedFibersAngle14And26d6_2000_150_p0d7_thick0d4doubl_thres1e-6_refined', '41')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1407: 2023-04-11 14:44:47
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1421: 2023-05-11 11:18:01
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1503: 2024-01-10 21:08:19
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranes.mdxv')
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Tools__System__Save_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesStraightFibers.mdxv')
Process.Model__CCF__011_FEM_Membranes('Model/CCF/011 FEM Solver', 'Model/CCF/08 Growth', 'Model/CCF/09 Pulling Force', '10e-6', 'Model/CCF/04 Stress-Strain', '1, 5, 7, 8', 'Mesh/System/Save', 'FibersGrowth_26_50_10_straightFibers_2000_150_p0d7_thick0d4doubl_thres1e-6', '9')
Process.Tools__System__Load_View('FemMembranes.mdxv')
######## New MorphoDynamX session v2.0 r2-1503: 2024-01-10 21:17:41
Process.Tools__System__Load_View('FemMembranes.mdxv')
Process.Tools__System__Load_View('/home/gabriella/cardaminecontractility_growth/oofs/ext/Multi_layer_Growth/FemMembranesCrossedFibers.mdxv')
Process.Model__CCF__17_Compute_Current_Directions('', 'Linear Triangle', 'Triangle Element', '1.e-6')
Process.Model__CCF__18_Visualize_Directions('Material Direction', 'Growth Direction', '1.0', '', 'Linear Triangle', 'Triangle Element', '1.e-6')
Process.Model__CCF__11_Vector_Render_Fiber1('Fiber1 Vector', 'Material Anisotropy', 'Max', '.5', '1.0', '.01', 'Fiber1 Triangle')
