// Make a block of 3D plant tissue
// Coordinate system (x,y,z) or ...
//                   (a,r,l) (angle, radius, length) for cylindrical

[Main]
Debug: 0
Epsilon: .00001
SegSz: 1 1 1 // size of segments

// The next section defines the cell layers, Go Wild!
// These are the cell sizes in segments
LayerSegs: 15 16 8
LayerSegs: 15 16 8
LayerSegs: 15 16 8
// Starting points of cells in segments
LayerStart: 0 0 0
LayerStart: 15 4 0
LayerStart: 30 0 0
// Ending points of cells in segments
LayerEnd: 15 80 8
LayerEnd: 30 84 8
LayerEnd: 45 80 8
// Make layers or not (good for debugging)
LayerMake: true
LayerMake: true
LayerMake: true

Stagger: 0
Stagger: 0
Stagger: 0
