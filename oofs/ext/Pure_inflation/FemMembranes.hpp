#ifndef FEM_MEMBRANES_HPP
#define FEM_MEMBRANES_HPP

#include <MDXProcessFem.hpp>
#include <MeshProcessSystem.hpp>

namespace mdx
{ 
  class FemMembraneComputeCurrentDirections;

  class FemMembranes : public Process
  {
  public:
    FemMembranes(const Process &proc) : Process(proc) 
    {
      setName("Model/CCF/01 Fem Membranes");
      setDesc("Fem Simulation of inflation and fix ends force");

      addParm("Fem Solver", "Name of Fem solver process", "Model/CCF/01 Fem Solver");
      addParm("Fem Extend", "Name of the extensometer process", "Model/CCF/09 Fem FixEnds");
    }
    bool initialize(QWidget *parent);
    bool step();
    bool rewind(QWidget *parent);

  private:
    fem::FemSolver *femSolver = 0;
    fem::ExtensometerDirichlet *femExtend = 0;
  };


  class FemMembraneSolver : public fem::FemSolver
  {
  public:
    FemMembraneSolver(const Process &proc) : FemSolver(proc) 
    {
      setName("Model/CCF/01 Fem Solver");
      setDesc("Fem solver process");

      // Update parameters with our own defaults
      setParmDefault("Stress-Strain", "Model/CCF/04 Stress-Strain");

      // Add derivatives processes
      addParm("Element Derivs", "Process for element derivatives", "Model/CCF/02 Triangle Derivs");
      addParm("Pressure Derivs", "Process for pressure derivatives", "Model/CCF/07 Pressure/01 Pressure Derivs");
      addParm("Dirichlet Derivs", "Process for Dirichlet derivatives", "Model/CCF/08 Dirichlet/01 Dirichlet Derivs");
   
    }
  };

  class FemMembraneDerivs : public fem::ElementDerivs
  {
  public:
    FemMembraneDerivs(const Process &proc) : ElementDerivs(proc) 
    {
      setName("Model/CCF/02 Triangle Derivs");

      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneRefCfg : public fem::SetRefCfg
  {
  public:
    FemMembraneRefCfg(const Process &proc) : SetRefCfg(proc) 
    {
      setName("Model/CCF/03 Reference Configuration");

      addParm("Thickness", "Thickness of the membrane elements", "1.0");
      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneStressStrain : public fem::StressStrain
  {
  public:
    FemMembraneStressStrain(const Process &proc) : StressStrain(proc) 
    {
      setName("Model/CCF/04 Stress-Strain");

      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneSetMaterial : public fem::SetTransIsoMaterial
  {
  public:
    FemMembraneSetMaterial(const Process &proc) : SetTransIsoMaterial(proc) 
    {
      setName("Model/CCF/05 Set Material Properties");
    }
  };

  class FemMembraneAnisoDir : public fem::SetAnisoDir
  {
  public:
    FemMembraneAnisoDir(const Process &proc) : SetAnisoDir(proc) 
    {
      setName("Model/CCF/06 Set Ansio Dir");

      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneSetPressure : public fem::SetPressure
  {
  public:
    FemMembraneSetPressure(const Process &proc) : SetPressure(proc) 
    {
      setName("Model/CCF/07 Pressure/00 Pressure");
    }
  };

  class FemMembranePressureDerivs : public fem::PressureDerivs
  {
  public:
    FemMembranePressureDerivs(const Process &proc) : PressureDerivs(proc) 
    {
      setName("Model/CCF/07 Pressure/01 Pressure Derivs");
    }
  };

  class FemMembraneSetDirichlet : public fem::SetDirichlet
  {
  public:
    FemMembraneSetDirichlet(const Process &proc) : SetDirichlet(proc) 
    {
      setName("Model/CCF/08 Dirichlet/00 Set Dirichlet");
    }
  };

  class FemMembraneDirichletDerivs : public fem::DirichletDerivs
  {
  public:
    FemMembraneDirichletDerivs(const Process &proc) : DirichletDerivs(proc) 
    {
      setName("Model/CCF/08 Dirichlet/01 Dirichlet Derivs");
    }
  };

  class FemMembraneExtend : public fem::ExtensometerDirichlet
  {
  public:
    FemMembraneExtend(const Process &proc) : fem::ExtensometerDirichlet(proc) 
    {
      setName("Model/CCF/09 Fem FixEnds");
      setDesc("Fem Simulation of force computation at fixed ends");

      setParmDefault("Fem Solver", "Model/CCF/01 Fem Solver");
      setParmDefault("Dirichlet Derivs", "Model/CCF/08 Dirichlet/01 Dirichlet Derivs");
    }
  };




  class FemMembraneVisMaterial : public fem::VisTransIsoMaterial
  {
  public:
    FemMembraneVisMaterial(const Process &proc) : VisTransIsoMaterial(proc) 
    {
      setName("Model/CCF/20 Visualization/00 Visualize Material");
    }
  };
  
  class FemMembraneComputeCurrentDirections : public fem::ComputeCurrentDirections
  {
    public:
      FemMembraneComputeCurrentDirections(const Process &proc) : ComputeCurrentDirections(proc)
      {
        setName("Model/CCF/21 Current Directions/b Compute Current Directions") ;
      }
  };

  class FemMembraneVisDirections : public fem::VisDirections
  {
    public:
      FemMembraneVisDirections(const Process &proc) : VisDirections(proc)
      {
        setName("Model/CCF/21 Current Directions/c Visualize Directions") ;
      }
  };


  class FemMembraneCurrentDirections : public Process
  {
    public:
      FemMembraneCurrentDirections(const Process &proc) : Process(proc)
      {
        setName("Model/CCF/21 Current Directions/a Cumulative Run");
        setDesc("Cumulative run to compute and visualize current anisotropy directions");
        addParm("Compute Current Direction process", "Name of the process to compute current directions", "Model/CCF/21 Current Directions/b Compute Current Directions");
        addParm("Visualize current direction process", "Name of the process to visualize current directions", "Model/CCF/21 Current Directions/c Visualize Directions");
      }
      
      bool run(); 
      FemMembraneComputeCurrentDirections *femMembraneComputeCurrDirection = 0;
      FemMembraneVisDirections *femMembraneVisDirections = 0;

  };


  class FemMembraneVisPressure : public fem::VisPressure
  {
  public:
    FemMembraneVisPressure(const Process &proc) : VisPressure(proc) 
    {
      setName("Model/CCF/20 Visualization/02 Visualize Pressure");
    }
  };
  
  class FemMembraneVisDirichlet : public fem::VisDirichlet
  {
  public:
    FemMembraneVisDirichlet(const Process &proc) : VisDirichlet(proc) 
    {
      setName("Model/CCF/20 Visualization/03 Visualize Dirichlet");
    }
  };


  class FemMembraneVisGrowth : public fem::VisGrowth
  {
  public:
    FemMembraneVisGrowth(const Process &proc) : VisGrowth(proc) 
    {
      setName("Model/CCF/20 Visualization/04 Visualize Growth");
    }
  };

}
#endif

