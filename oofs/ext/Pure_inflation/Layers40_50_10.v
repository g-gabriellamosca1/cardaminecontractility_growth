// Make a block of 3D plant tissue
// Coordinate system (x,y,z) or ...
//                   (a,r,l) (angle, radius, length) for cylindrical

[Main]
Debug: 0
Epsilon: .00001
SegSz: 1 1 1 // size of segments

// The next section defines the cell layers, Go Wild!
// These are the cell sizes in segments
LayerSegs: 40 50 10

// Starting points of cells in segments
LayerStart: 0 0 0
// Ending points of cells in segments
LayerEnd: 40 250 10

// Make layers or not (good for debugging)
LayerMake: true

Stagger: 0
