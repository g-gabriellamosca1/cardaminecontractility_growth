#include "FemMembranes.hpp"

namespace mdx
{


  bool FemMembranes::initialize(QWidget *parent)
  {
    // Get the solver process
    if(parm("Fem Solver").isEmpty())
      throw QString("%1::initialize Solver process empty").arg(name());
    if(!getProcess(parm("Fem Solver"), femSolver))
      throw QString("%1::initialize Unable to make solver process: %2").arg(name()).arg(parm("Fem Solver"));
    femSolver->initialize(parent);

    if(parm("Fem Extend").isEmpty())
      throw QString("%1::initialize Extend process empty").arg(name());
    if(!getProcess(parm("Fem Extend"), femExtend))
      throw QString("%1::initialize Unable to make extensometer process: %2").arg(name()).arg(parm("Fem Extend"));
    femExtend->initialize(parent);


    return true;
  }

  bool FemMembranes::step()
  {

    if(!femSolver)
      throw QString("%1:step Solver process not set").arg(name());
    if(!femExtend)
      throw QString("%1:step Extend process not set").arg(name());

    // Run the fem simulation
    bool converged = !femSolver->step();
    if(converged)
      return femExtend->step();
      //return femIndent->step();

    return true;
  }

  bool FemMembranes::rewind(QWidget *parent)
  {
    // To rewind, we'll reload the mesh
    Mesh *mesh = currentMesh();
    if(!mesh or mesh->file().isEmpty())
      throw(QString("No current mesh, cannot rewind"));
    MeshLoad meshLoad(*this);
    meshLoad.setParm("File Name", mesh->file());

    return meshLoad.run();
  }

  bool FemMembraneCurrentDirections::run()
  {
    if(!getProcess(parm("Compute Current Direction process"), femMembraneComputeCurrDirection))
      throw(QString("FemMembraneCurrentDirections::run Unable to make process: %1").arg(parm("Compute Current Direction process")));
    femMembraneComputeCurrDirection->run();

    if(!getProcess(parm("Visualize current direction process"), femMembraneVisDirections))
      throw(QString("FemMembraneCurrentDirections::run Unable to make process: %1").arg(parm("Visualize current direction process")));
    femMembraneVisDirections->run();
    return true;
  }



  REGISTER_PROCESS(FemMembranes);
  REGISTER_PROCESS(FemMembraneSolver);
  REGISTER_PROCESS(FemMembraneRefCfg);
  REGISTER_PROCESS(FemMembraneStressStrain);
  REGISTER_PROCESS(FemMembraneDerivs);
  REGISTER_PROCESS(FemMembraneSetMaterial);
  REGISTER_PROCESS(FemMembraneAnisoDir);
  REGISTER_PROCESS(FemMembraneSetPressure);
  REGISTER_PROCESS(FemMembranePressureDerivs);
  REGISTER_PROCESS(FemMembraneVisMaterial);
  REGISTER_PROCESS(FemMembraneVisDirections);
  REGISTER_PROCESS(FemMembraneComputeCurrentDirections);
  REGISTER_PROCESS(FemMembraneCurrentDirections);
  REGISTER_PROCESS(FemMembraneSetDirichlet);
  REGISTER_PROCESS(FemMembraneDirichletDerivs);
  REGISTER_PROCESS(FemMembraneExtend);

  REGISTER_PROCESS(FemMembraneVisPressure);
  REGISTER_PROCESS(FemMembraneVisDirichlet);
  REGISTER_PROCESS(FemMembraneVisGrowth);
}
