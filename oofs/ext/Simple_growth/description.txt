Mesh generation: MMX from segmentation, created with 3D mesh, 1um^3 cube size, 3 smoothign passes, then 3D mesh. Eliminated volumes smaller than 20um^3

Simulation set up: 
-thickness 0.2 um
-Ey = 100 Ex 100
nu = 0.3
Pressure 1 MPa

Convergence set to 1e-12 (Went full newton)
